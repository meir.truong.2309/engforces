
from django.db import models


class English(models.Model):
    english = models.CharField(max_length = 50)

class Vietnamese(models.Model):
    vietnamese = models.CharField(max_length = 50)

class Words(models.Model):
    en = models.ForeignKey(English, on_delete=models.CASCADE)
    vi = models.ForeignKey(Vietnamese, on_delete=models.CASCADE)

class Synonyms(models.Model):
    en1 = models.ForeignKey(English, on_delete=models.CASCADE, related_name= "en1")
    en2 = models.ForeignKey(English, on_delete=models.CASCADE, related_name= "en2")
    vi = models.ForeignKey(Vietnamese, on_delete=models.CASCADE)
