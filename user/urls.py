from django.urls import path
from user.api.register import UserRegisterView
from .token.MyTokenObtainPairView import MyTokenObtainPairView
urlpatterns = [
    path('register/', UserRegisterView.as_view() , name = 'user_register'),
    path('login/' , MyTokenObtainPairView.as_view(), name = 'login')
]