from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework_simplejwt.views import TokenObtainPairView


class MyTokenObtainPairSerializer(TokenObtainPairSerializer):
    default_error_messages = {
        'no_active_account': {
            'error_code': '400',
            'error_message': 'user không tồn tại',
            'data' : None
        }
    }   
    def validate(self, attrs):
        data = super().validate(attrs)
        refresh = self.get_token(self.user)
        del data['refresh'] , data['access']
        data['error_code'] = '0'
        data['error_message'] = 'success'

        data['data'] = {}
        

        # Add extra responses here
        data['data']['username'] = self.user.username
        data['data']['email'] = self.user.email
        data['data']['date_joined'] = self.user.date_joined
        data['data']['last_login'] = self.user.last_login
        
        if self.user.is_admin:
            data['data']['role']  = 'admin'
        elif self.user.is_staff and not self.user.is_admin:
            data['data']['role'] = 'staff'
        else:
            data['data']['role'] = 'user'


        data['data']['token'] = {}
        data['data']['token']['refresh'] = str(refresh)
        data['data']['token']['access'] = str(refresh.access_token)
        return data


class MyTokenObtainPairView(TokenObtainPairView):
    serializer_class = MyTokenObtainPairSerializer